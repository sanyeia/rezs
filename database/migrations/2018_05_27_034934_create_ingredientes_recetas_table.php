<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientesRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredientes_recetas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_receta')->unsigned();
            $table->string('nombre');
            $table->integer('cantidad');
            $table->string('tipo_cantidad')->nullable();
            $table->timestamps();
        });
        
        Schema::table('ingredientes_recetas', function (Blueprint $table) {
            $table->foreign('id_receta')->references('id')->on('recetas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ingredientes_recetas', function (Blueprint $table) {
            $table->dropForeign('ingredientes_recetas_id_receta_foreign');
        });
        Schema::dropIfExists('ingredientes_recetas');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->string('nick');
            $table->string('password');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('foto')->nullable();
            $table->string('email')->unique();
            $table->enum('rango', ['Aprendiz', 'Cocinero', 'Sous Chef', 'Chef'])->default('Aprendiz');

            $table->primary('nick');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}

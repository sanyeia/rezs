<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recetas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_nick');
            $table->string('nombre');
            $table->string('foto')->nullable();
            $table->integer('id_categoria')->unsigned();
            $table->text('preparacion', 1000);
            $table->integer('calificacion')->default(0);

            $table->timestamps();
        });
        
        Schema::table('recetas', function (Blueprint $table) {
            $table->foreign('user_nick')->references('nick')->on('usuarios');
            $table->foreign('id_categoria')->references('id')->on('categorias_recetas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recetas', function (Blueprint $table) {
            $table->dropForeign('recetas_user_nick_foreign');
            $table->dropForeign('recetas_id_categoria_foreign');
        });
        Schema::dropIfExists('recetas');
    }
}

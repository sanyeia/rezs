<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSugerenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sugerencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_receta')->unsigned();
            $table->string('user_nick');
            $table->text('sugerencia');

            $table->timestamps();
        });

        Schema::table('sugerencias', function (Blueprint $table) {
            $table->foreign('id_receta')->references('id')->on('recetas');
            $table->foreign('user_nick')->references('nick')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sugerencias', function (Blueprint $table) {
            $table->dropForeign('sugerencias_id_receta_foreign');
            $table->dropForeign('sugerencias_user_nick_foreign');
        });
        Schema::dropIfExists('sugerencias');
    }
}

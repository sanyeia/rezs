<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_receta')->unsigned();
            $table->string('user_nick');
            $table->text('comentario');
            $table->integer('calificacion')->default(0);

            $table->timestamps();
        });

        
        Schema::table('comentarios', function (Blueprint $table) {
            $table->foreign('id_receta')->references('id')->on('recetas');
            $table->foreign('user_nick')->references('nick')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comentarios', function (Blueprint $table) {
            $table->dropForeign('comentarios_id_receta_foreign');
            $table->dropForeign('comentarios_user_nick_foreign');
        });
        Schema::dropIfExists('comentarios');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosSeguidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_seguidos', function (Blueprint $table) {
            $table->string('user');
            $table->string('follow');
            $table->primary(['user', 'follow']);
            $table->timestamps();
        });

        Schema::table('usuarios_seguidos', function (Blueprint $table) {
            $table->foreign('user')->references('nick')->on('usuarios');
            $table->foreign('follow')->references('nick')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuarios_seguidos', function (Blueprint $table) {
            $table->dropForeign('usuarios_seguidos_user_foreign');
            $table->dropForeign('usuarios_seguidos_follow_foreign');
        });
        Schema::dropIfExists('usuarios_seguidos');
    }
}
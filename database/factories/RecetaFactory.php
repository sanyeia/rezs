<?php

use Faker\Generator as Faker;
use App\Receta;
use App\Usuario;
use App\CategoriaReceta;

$factory->define(Receta::class, function (Faker $faker) {
    return [
        'user_nick' => Usuario::all()->random()->nick,
        'nombre' => $faker->sentence($nbWords = 4, $variableNbWords = true) ,
        'foto' => $faker->imageUrl(800, 600, 'food'),
        'id_categoria' => CategoriaReceta::all()->random()->id,
        'preparacion' => $faker->paragraph($nbSentences = 6, $variableNbSentences = true),
    ];
});

<?php

use Faker\Generator as Faker;
use App\Comentario;
use App\Receta;
use App\Usuario;

$factory->define(Comentario::class, function (Faker $faker) {
    return [
        'id_receta' => Receta::all()->random()->id,
        'user_nick' => Usuario::all()->random()->nick,
        'comentario' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
        'calificacion' => $faker->numberBetween($min = 1, $max = 5)
    ];
});

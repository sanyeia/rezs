<?php

use Faker\Generator as Faker;
use App\Sugerencia;
use App\Usuario;
use App\Receta;

$factory->define(Sugerencia::class, function (Faker $faker) {
    return [
        'id_receta' => Receta::all()->random()->id,
        'user_nick' => Usuario::all()->random()->nick,
        'sugerencia' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
    ];
});

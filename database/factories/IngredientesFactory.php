<?php

use Faker\Generator as Faker;
use App\IngredientesReceta;
use App\Receta;

$factory->define(IngredientesReceta::class, function (Faker $faker) {
    return [
        'id_receta' => Receta::all()->random()->id,
        'nombre' => $faker->word,
        'cantidad' => $faker->randomDigit,
        'tipo_cantidad' => $faker->word
    ];
});

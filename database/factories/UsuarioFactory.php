<?php

use Faker\Generator as Faker;
use App\Usuario;

$factory->define(Usuario::class, function (Faker $faker) {
    return [
        'nick' => $faker->userName,
        'password' => 'secret',
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'foto' => $faker->imageUrl(800, 600, 'people'),
    ];
});

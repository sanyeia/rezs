<?php

use Illuminate\Database\Seeder;
use App\UsuariosSeguido;
use App\Usuario;

class pruebas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $till = 30;
        for ($i=0; $i < $till ; $i++) { 

            $x = Usuario::all()->random(2);
            if ( UsuariosSeguido::where('user', $x[0]->nick)->where('follow',$x[1]->nick)->exists() ) {
                $i--;
            } else {
                UsuariosSeguido::create([
                    'user' => $x[0]->nick,
                    'follow' => $x[1]->nick,
                ]);
            }
            
        }

    }
}

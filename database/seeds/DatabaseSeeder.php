<?php

use Illuminate\Database\Seeder;
use App\CategoriaReceta;
use App\Sugerencia;
use App\Usuario;
use App\Comentario;
use App\UsuariosSeguido;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriaSeeder::class);

        factory(Usuario::class, 10)->create();
        
        $this->call(RecetaSeeder::class);
        
        factory(Comentario::class, 40)->create();
        factory(Sugerencia::class, 15)->create();
        
        $till = 30;
        for ($i=0; $i < $till ; $i++) { 

            $x = Usuario::all()->random(2);
            if ( UsuariosSeguido::where('user', $x[0]->nick)->where('follow',$x[1]->nick)->exists() ) {
                $i--;
            } else {
                UsuariosSeguido::create([
                    'user' => $x[0]->nick,
                    'follow' => $x[1]->nick,
                ]);
            }
            
        }
    }
}

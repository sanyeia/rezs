<?php

use App\IngredientesReceta;
use App\Receta;
use App\Usuario;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\CategoriaReceta;

class RecetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $user = Usuario::create([
            'nick' => 'jdoe02',
            'password' => 'secret',
            'nombre' => 'john',
            'apellido' => 'doe',
            'fecha_nacimiento' => $faker->dateTimeThisCentury,
            'email' => $faker->unique()->safeEmail,
        ]);

        $r1 = Receta::create([
            'user_nick' => $user->nick,
            'nombre' => 'Carne de hamburguesa de cerdo',
            'foto' => 'http://www.keken.com.mx/images/Carne%20Hamburguesa%202.jpg',
            'id_categoria' => CategoriaReceta::all()->random()->id,
            'preparacion' => 'En un bowl grande, mezcla todos los ingredientes para preparar la carne para hamburguesas. Con las manos húmedas, forma bolas de 5 a 6 cm de diámetro. Colócalas en una bandeja y deja reposar en el refrigerador por 60 minutos o mientras preparas los acompañamientos. Con una superficie plana como la base de un plato, aplana las bolas de carne para formar las hamburguesas y cocínalas en un sartén con un poco de aceite por 4 a 5 minutos de cada lado. Prepara la salsa licuando todos los ingredientes. Comprueba la cocción y sirve las hamburguesas sobre los panes. Agrega la salsa sobre cada hamburguesa, añade lechuga, tomate y cebolla y tus aderezos favoritos. Acompaña con papas chips, a la francesa o en gajos.',
        ]);

        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'aceite de maíz',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'cebolla',
            'cantidad' => 1,
            'tipo_cantidad' => '',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'kétchup',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'vinagre',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'azúcar negra',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'miel',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'humo líquido',
            'cantidad' => 1,
            'tipo_cantidad' => 'cucharas',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'salsa inglesa',
            'cantidad' => 2,
            'tipo_cantidad' => 'cucharas',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'mostaza',
            'cantidad' => 1,
            'tipo_cantidad' => 'cucharas',
        ]);
        IngredientesReceta::create([
            'id_receta' => $r1->id,
            'nombre' => 'sal',
            'cantidad' => 1,
            'tipo_cantidad' => 'pizca',
        ]);

        $r2 = Receta::create([
            'user_nick' => $user->nick,
            'nombre' => 'Tortitas de cerdo a la Romana',
            'foto' => 'http://www.keken.com.mx/images/tortitas%20romana%20(450x350px).jpg',
            'id_categoria' => CategoriaReceta::all()->random()->id,
            'preparacion' => 'Lava y desinfecta todos los vegetales. En un sartén con un poco de aceite, sofríe el ajo y la cebolla picada finamente. Mezcla el sofrito, con la carne molida de cerdo, en un recipiente plástico. Agrega sal y pimienta al gusto, las yemas de huevo y el cilantro picado y el queso parmesano rallado. Licua los tomates, con el puré de tomate y el caldo de pollo. Sazona la salsa con orégano, sal y pimienta al gusto y cocina en una cacerola ancha, con 2 cucharaditas de aceite vegetal. Forma tortitas con la carne, de aproximadamente 6 cm. de diámetro y  2 cm. de grosor. Pásalas por harina de trigo y séllalas en un sartén con aceite vegetal, 1 minuto de cada lado, a fuego alto. Pon las tortitas a cocer dentro de la salsa, a fuego bajo, por 15 minutos, o hasta que estén cocidas por dentro. Sirve acompañadas de arroz blanco.'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'Carne Molida de Cerdo',
            'cantidad' => 1 ,
            'tipo_cantidad' =>'kg' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'cilantro',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'dientes de ajo picados finamente',
            'cantidad' => 3,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'tomates saladet',
            'cantidad' => 6,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'puré de tomate',
            'cantidad' => 1,
            'tipo_cantidad' =>'taza' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'huevo',
            'cantidad' => 2,
            'tipo_cantidad' =>'' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'harina de trigo',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'aceite vegetal',
            'cantidad' => 5,
            'tipo_cantidad' =>'cucharas' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r2->id,
            'nombre' => 'orégano',
            'cantidad' => 1,
            'tipo_cantidad' =>'pizca' 
        ]);
        
        
        $r3 = Receta::create([
            'user_nick' => $user->nick,
            'nombre' => 'Tacos de Bistec de Res',
        'foto' => 'http://www.keken.com.mx/images/tacos%20de%20bistec%203(450x350px).jpg',
            'id_categoria' => CategoriaReceta::all()->random()->id,
            'preparacion' =>'Lava y desinfecta todos los vegetales. Extiende los bistecs en una superficie plana y sazónalos con sal y pimienta. Calienta un sartén y fríe los bistecs con un poco de aceite, cocinándolos por 90 segundos de cada lado, para que no se sequen. Prepara la crema de ajo, colocando en un bowl los 2 huevos, los ajos, el limón, la sal y pimienta al gusto; añade 3 cucharadas de aceite y comienza a batir con un multipractic. Cuando el huevo se integre con el aceite, comienza a agregar el resto del aceite, muy lentamente, en forma de un hilo delgado, sin parar de batir, hasta lograr la consistencia deseada. Pica la carne y sirve sobre tortillas de harina. Agrega trozos de Chicharrón de Cerdo y la crema de ajo encima de la carne. Espolvorea cilantro picado.'
        ]);
        
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'Bistec de Res',
            'cantidad' => 1,
            'tipo_cantidad' => 'kg'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'Chicharrón de Cerdo',
            'cantidad' => 200,
            'tipo_cantidad' => 'gramos'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'cilantro',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'aceite vegetal',
            'cantidad' => 3,
            'tipo_cantidad' => 'cucharas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'dientes de ajo',
            'cantidad' => 5,
            'tipo_cantidad' => ''
        ]);
        
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'aceite vegetal',
            'cantidad' => 2,
            'tipo_cantidad' => 'taza' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'huevos',
            'cantidad' => 2,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r3->id,
            'nombre' => 'jugo de limón',
            'cantidad' => 1,
            'tipo_cantidad' => 'cucharas'
        ]);
        
        $r4 = Receta::create([
            'user_nick' => $user->nick,
            'nombre' => 'Filetitos de Cerdo empanizados',
            'foto' => 'http://www.keken.com.mx/images/73_350x231.jpg',
            'id_categoria' => CategoriaReceta::all()->random()->id,
            'preparacion' => 'Corta el filete de cerdo en medallones regulares, de 1 cm de grosor. Sazona los trozos con una mezcla de sal, pimienta y el jugo de lima o limón. Sazona la harina con el orégano, la paprika, el ajo en polvo y un poco de sal. Enharina los trozos de carne, un par a la vez, hasta que queden completamente cubiertos. Bate los huevos en un bowl y reserva. Tritura ligeramente las hojuelas de Korn Flakes, en una bolsa plástica gruesa, mézclalas con el queso parmesano rallado y extiéndelas sobre una bandeja. Calienta el aceite vegetal en un sartén hondo, o una olla pequeña, a fuego medio. Sumerge los trozos de carne en el huevo batido, e inmediatamente después, empaniza con las hojuelas de maíz; procura presionar firmemente el trozo, para que el empanizado se adhiera correctamente. Fríe en abundante aceite vegetal, a fuego medio, para que las piezas se cocinen correctamente hasta dorar. Puedes acompañar con papas fritas y ensalada de arúgula y espinacas.'
        ]);
        
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'Filete de Cerdo',
            'cantidad' => 1,
            'tipo_cantidad' => 'kg' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'jugo de lima' ,
            'cantidad' => 1,
            'tipo_cantidad' => 'taza' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' =>'harina de trigo' ,
            'cantidad' => 1,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'huevos',
            'cantidad' => 4,
            'tipo_cantidad' => ' '
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'Korn Flakes',
            'cantidad' => 2,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'aceite vegetal',
            'cantidad' => 1,
            'tipo_cantidad' => 'lt'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'orégano',
            'cantidad' => 1,
            'tipo_cantidad' => 'cuchara' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'paprika',
            'cantidad' => 1,
            'tipo_cantidad' => 'cuchara' 
        ]);
        IngredientesReceta::create([
            'id_receta' => $r4->id,
            'nombre' => 'ajo en polvo',
            'cantidad' => 1,
            'tipo_cantidad' => 'cuchara' 
        ]);

        $r5 = Receta::create([
            'user_nick' => $user->nick,
            'nombre' => 'Milanesa de pollo a la Cordon Blue',
            'foto' => 'http://www.keken.com.mx/images/pechuga%20cordon%20blue%201(450x350px).jpg',
            'id_categoria' => CategoriaReceta::all()->random()->id,
            'preparacion' => 'Pon a calentar el aceite en cazuela ancha, a fuego bajo. Extiende las milanesas de pollo, en una superficie plana y sazona con el jugo de limón, sal y pimienta. Coloca encima de ellas, una rebanada de jamón y otra de queso. Forma un rollo con cada una, corta las orillas y asegura con palillos de dientes. Bate los huevos, sazónalos con sal y pimienta y colócalos en un recipiente ancho. Extiende la harina en una bandeja plana. Mezcla el cereal molido, con el queso parmesano y el orégano, poniendo la mezcla, en otra bandeja plana. Pasa los rollos por harina, luego por huevo y empaniza con el cereal. Fríe a temperatura baja, de 5 a 6 minutos, hasta que el rollo este dorado. Comprueba la cocción, partiendo al centro del rollo. Acompaña con ensalada verde y puré de papas.' 
        ]);

        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'Milanesa de Pollo',
            'cantidad' => 750 ,
            'tipo_cantidad' => 'gramos'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'jamón de cerdo',
            'cantidad' => 6,
            'tipo_cantidad' => 'rebanadas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'queso tipo manchego',
            'cantidad' => 6,
            'tipo_cantidad' => 'rebanadas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'huevos',
            'cantidad' => 4,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'pan molido',
            'cantidad' => 2,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'huevos',
            'cantidad' => 4,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'queso parmesano rallado',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'Harina de trigo',
            'cantidad' => 2,
            'tipo_cantidad' => 'tazas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'jugo de limón',
            'cantidad' => 2,
            'tipo_cantidad' => 'cucharas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'hojas de orégano',
            'cantidad' => 1,
            'tipo_cantidad' => 'cucharas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'aceite vegetal',
            'cantidad' => 2,
            'tipo_cantidad' => 'tazas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r5->id,
            'nombre' => 'Palillos de dientes',
            'cantidad' => 5,
            'tipo_cantidad' => 'unidades'
        ]);
        
        $r6 = Receta::create([
            'user_nick' => $user->nick,
            'nombre' => 'Chop Suey de Cerdo',
            'foto' => 'http://www.keken.com.mx/images/chop%20suey%204%20(4).jpg',
            'id_categoria' => CategoriaReceta::all()->random()->id,
            'preparacion' =>'Corta la carne de cerdo, en cubos regulares de 2 cm por lado, sazónalos con sal y pimienta. Parte a la mitad el pimiento y retira el corazón y las semillas, pícalo en cubos. Pela la zanahoria y pícala en tiras finas. Separa los brócolis en pequeños racimos. Pela las tiras de apio y córtalas diagonalmente en trozos pequeños. Retira los tallos de las cebollitas cambray, pícalos finamente y resérvalos. Corta las cebollitas en tiras finas.  Diluye la maicena en el caldo y ponlo a hervir en un wok (o en una cazuela profunda) Agrega la miel, la salsa de soya, el jengibre y los trozos de carne; cocina a fuego alto, por 10 minutos. Agrega los pimientos y los brócolis y la zanahoria; continúa cocinando a fuego alto por 5 minutos más. Añade el apio y las cebollitas cambray y cocina por 3 minutos antes de agregar los brotes de soja. Remueve y apaga el fuego. Sazona con más soya si es necesario. Sirve sobre un poco de arroz blanco y espolvorea con los tallos de las cebollitas picados.' 
        ]);

        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'Recorte 80/20 de Cerdo',
            'cantidad' => 1,
            'tipo_cantidad' => 'kg'
        ]);

        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'pimiento rojo',
            'cantidad' => 1,
            'tipo_cantidad' => ''
        ]);

        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'zanahoria',
            'cantidad' => 1,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'brócoli',
            'cantidad' => 1,
            'tipo_cantidad' => 'pieza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'apio',
            'cantidad' => 2,
            'tipo_cantidad' => 'tiras'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'cebollitas cambray',
            'cantidad' => 3,
            'tipo_cantidad' => ''
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'brotes de soya',
            'cantidad' => 200,
            'tipo_cantidad' => 'gramos'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'miel de maíz',
            'cantidad' => 3,
            'tipo_cantidad' => 'cucharas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'salsa de soya',
            'cantidad' => 5,
            'tipo_cantidad' => 'cucharas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'maicena',
            'cantidad' => 1,
            'tipo_cantidad' => 'cucharas'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'caldo de pollo',
            'cantidad' => 1,
            'tipo_cantidad' => 'taza'
        ]);
        IngredientesReceta::create([
            'id_receta' => $r6->id,
            'nombre' => 'jengibre rallado',
            'cantidad' => 1,
            'tipo_cantidad' => 'cucharas'
        ]);

        factory(Receta::class, 20)->create();
        factory(IngredientesReceta::class, 50)->create();

    }
}

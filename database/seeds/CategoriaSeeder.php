<?php

use Illuminate\Database\Seeder;
use App\CategoriaReceta;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoriaReceta::create([
            'nombre'=> 'Aperitivos y Entremeses',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac finibus leo. Nullam aliquam lorem nec turpis porttitor auctor. Aenean imperdiet leo ac dui porttitor, ac dictum turpis commodo.'
        ]);
        
        CategoriaReceta::create([
            'nombre'=> 'Bebidas y Cócteles',
            'descripcion' => 'Vestibulum scelerisque in lectus et ullamcorper. Sed venenatis hendrerit odio sit amet consectetur. '
        ]);

        CategoriaReceta::create([
            'nombre'=> 'Cocina Internacional',
            'descripcion' => 'Vestibulum scelerisque in lectus et ullamcorper. Sed venenatis hendrerit odio sit amet consectetur. '
        ]);

        CategoriaReceta::create([
            'nombre'=> 'Postres, Dulces y Tartas',
            'descripcion' => 'Vestibulum scelerisque in lectus et ullamcorper. Sed venenatis hendrerit odio sit amet consectetur. '
        ]);

        CategoriaReceta::create([
            'nombre'=> 'Cremas y Sopas',
            'descripcion' => 'Vestibulum scelerisque in lectus et ullamcorper. Sed venenatis hendrerit odio sit amet consectetur. '
        ]);

        CategoriaReceta::create([
            'nombre'=> 'Recetas Sanas',
            'descripcion' => 'Vestibulum scelerisque in lectus et ullamcorper. Sed venenatis hendrerit odio sit amet consectetur. '
        ]);

        CategoriaReceta::create([
            'nombre'=> 'Cocina rápida',
            'descripcion' => 'Vestibulum scelerisque in lectus et ullamcorper. Sed venenatis hendrerit odio sit amet consectetur. '
        ]);
    }
}

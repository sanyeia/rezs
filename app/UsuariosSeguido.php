<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosSeguido extends Model
{
    protected $table = 'usuarios_seguidos';
    public $incrementing = false;
}

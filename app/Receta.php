<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $table = 'recetas';
    protected $fillable = [
        'nombre',
        'id_categoria',
        'preparacion'
    ];

    public function comentarios(){
        return $this->hasMany('App\Comentario', 'id_receta');
    }

    public function sugerencias(){
        return $this->hasMany('App\Sugerencia', 'id_receta');
    }

    public function ingredientes(){
        return $this->hasMany('App\IngredientesReceta', 'id_receta');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'user_nick');
    }
}

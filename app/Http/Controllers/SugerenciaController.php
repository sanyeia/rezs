<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;
use App\Usuario;
use App\Sugerencia;

class SugerenciaController extends Controller
{
    /**
     * registra una nueva sugerencia
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Receta $recipe, Usuario $user)
    {
        if ($request->has('sugerencia')) {
            $s = new Sugerencia();
            $s->id_receta = $recipe->id;
            $s->user_nick = $user->nick;
            $s->fill($request->sugerencia);
            $s->save();
            return response($s);
        } else {
            return response('Bad Request', 400);
        }
        
    }

    /**
     * muestra las sugerecias de una receta
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Receta $recipe)
    {
        return response($recipe->sugerencias);
    }
}

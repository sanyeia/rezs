<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Usuario;
use App\Receta;
use App\Photo;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['store','check', 'show']);
    }

    public function index() {
        $usuario = Usuario::all();
        return response($usuario);
    }

    public function store(Request $request) {
        if($request->has('user')){
            $user = new Usuario();
            $user->fill($request->user);
            $user->save();

            if($request->has('image')){
                $photo = new Photo();
                $photo->element_id = $user->nick;

                $imageResult = $this->imageStorage(Uuid::generate(4)->string, $request->image);

                $photo->path = $imageResult['path'];
                $photo->id = $imageResult['uuid'];
                $photo->url = $imageResult['url'];
                $photo->active = true;
                $photo->save();

                $user->foto = $photo->url;
            }

            $user->save();
            return response($user);
        }else{
            return response('Bad Request', 400);
        }
    }

    public function show(Request $r, Usuario $user) {
        if($r->header('nick') != null){
            if (Usuario::where('nick', $r->header('nick'))->exists()) {
                $me = Usuario::find($r->header('nick'))->first();
                $result = [
                    'Usuario:' => $user,
                    'Seguidores' => $user->followers->count(),
                    'Siguiendo' => $user->following->count(),
                    'NumRecetas' => Receta::where('user_nick', $user->nick)->count(),
                    'lo_sigo' => $me->following->contains($user),
                    'me_sigue' => $me->followers->contains($user),
                ];
            } else {
                return response('Bad Request', 400);
            }
            
        } else {
            $result = [
                'Usuario:' => $user,
                'Seguidores' => $user->followers->count(),
                'Siguiendo' => $user->following->count(),
                'NumRecetas' => Receta::where('user_nick', $user->nick)->count(),
            ];
        }

        return response($result);
    }

    public function update(Request $request, Usuario $user) {
        if($request->has('user')){
            $user->fill($request->user);
            $user->save();
            return response($user);
        }else{
            return response('Bad Request', 400);
        }
    }

    public function destroy(Usuario $user) {
        foreach ($user->quejas as $queja) {
            $queja->delete();
        }
        foreach ($user->sugerecias as $sugerecia) {
            $sugerecia->delete();
        }
        foreach ($user->comentarios as $comentario) {
            $comentario->delete();
        }
        foreach ($user->recetas as $receta) {
            foreach ($receta->ingredientes as $ingrediente) {
                $ingrediente->delete();
            }
            $receta->delete();
        }
        $user->delete();
        return response($user);
    }

    public function check($user) {
        $result = Usuario::select('nick', 'email')->where('nick', 'LIKE', '%'.$user.'%')
            ->orWhere('email', 'LIKE', '%'.$user.'%')
            ->first();
        if($result != null){
            return response($result);
        }
        return response('Nuevo');
    }
}

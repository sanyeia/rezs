<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Receta;
use App\IngredientesReceta;
use App\CategoriaReceta;
use Illuminate\Database\Eloquent\Collection;
use App\Traits\ApiResponser;
use ArrayObject;
use App\Comentario;
use App\Sugerencia;
use App\Photo;
use Webpatser\Uuid\Uuid;

class RecetaController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index','show','searchRecipe', 'userRecepies', 'categoryRecepies']);
    }

    /**
     * lista todas las recetas.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $recetas = Receta::all()->orderBy('created_at', 'desc');
        return $this->showAll($recetas);
    }

    /**
     * crea una nueva receta.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Usuario  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Usuario $user) {
        if($request->has('recipe') && $request->has('ingredients')){
            $recipe = new Receta();
            $recipe->user_nick = $user->nick;
            $recipe->fill($request->recipe);
            $recipe->save();
            if($recipe != false){
                foreach ($request->ingredients as $ingredient) {
                    $RecipeIngred = new IngredientesReceta();
                    $RecipeIngred->id_receta = $recipe->id;
                    $RecipeIngred->fill($ingredient);
                    $RecipeIngred->save();
                    if ($RecipeIngred == null) {
                        $recipe->recetas->delete();
                        $recipe->delete();
                        return response('Bad Request', 400);
                    }
                }
            } else {
                return response('Bad Request', 400);
            }

            if($request->has('image')){
                $photo = new Photo();
                $photo->element_id = $user->nick;

                $imageResult = $this->imageStorage(Uuid::generate(4)->string, $request->image);

                $photo->path = $imageResult['path'];
                $photo->id = $imageResult['uuid'];
                $photo->url = $imageResult['url'];
                $photo->active = true;
                $photo->save();

                $recipe->foto = $photo->url;
                $recipe->save();
            }
            return response($recipe);
        } else{
            return response('Bad Request', 400);
        }
    }

    /**
     * muestra una receta en especifico. (y por ahora sus comentarios y sugerencias tambien)
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Receta  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Receta $recipe) {
        $recipe->categoria = CategoriaReceta::where('id',$recipe->id_categoria)->first()->nombre;
        $ingredient = $recipe->ingredientes;
        $recipe->usuario;
        $result = [
            'receta' => $recipe, 
            'Num_comentarios' => $recipe->comentarios->count(),
            'Num_sugerencias' => $recipe->sugerencias->count(),
        ];
        return response($result);
    }

    /**
     * busca una receta por nombre
     * 
     * @param  $recipe (cadena con el nombre de la receta)
     * @return \Illuminate\Http\Response
     */
    public function searchRecipe($recipe) {
        $RecipeList =  Receta::where('nombre', 'LIKE', '%'.$recipe.'%')->orderBy('created_at')->get();
		return $this->showAll($RecipeList);
    }
    
    /**
     * busca un ingrediente por nombre
     * 
     * @param  $recipe (cadena con el nombre del ingrediente)
     * @return \Illuminate\Http\Response
     */
    public function searchIngredient($ingredient) {
        $IngreList =  IngredientesReceta::where('nombre', 'LIKE', '%'.$ingredient.'%')->get();
		return response($IngreList);
    }

    /**
     * actualiza la informacion de una receta receta.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Receta  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receta $recipe) {
        if($request->has('recipe') && $request->has('ingredients')){
            $recipe->fill($request->recipe);
            $recipe->save();
            if($recipe != false){
                foreach ($request->ingredients as $ingredient) {
                    $RecipeIngred = IngredientesReceta::where([
                            ['id_receta',$recipe->id],
                            ['nombre',$ingredient->nombre]
                        ])
                        ->first();
                    if ($RecipeIngred != null) {
                        $RecipeIngred->fill($ingredient);
                    } else {
                        $RecipeIngred = new IngredientesReceta();
                        $RecipeIngred->id_receta = $recipe->id;
                        $RecipeIngred->fill($ingredient);
                        $RecipeIngred->save();
                    }
                }
            } else {
                return response('Bad Request', 400);
            }
            return response($recipe);
        } else{
            return response('Bad Request', 400);
        }
    }

    /**
     * elimina una receta y todo lo relacionado con ella.
     * 
     * @param  App\Receta  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receta $recipe) {
        foreach ($recipe->ingredientes as $ingrediente) {
            $ingrediente->delete();
        }
        foreach ($recipe->comentarios as $comentario) {
            $comentario->delete();
        }
        foreach ($recipe->sugerencias as $sugerencia) {
            $sugerencia->delete();
        }
        $recipe->delete();
        return response($recipe);
    }

    /**
     * elimina un ingrediente de una receta
     * 
     * @param  App\IngredientesReceta  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroyIngredient(IngredientesReceta $ingredient) {
        $ingredient->delete();
        return $this->showOne($ingredient);
    }

    /**
     * recetas de un usuario
     * 
     * @param  App\IngredientesReceta  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function userRecepies(Usuario $user) {
        $result = [];
        foreach ($user->recetas()->orderBy('created_at', 'desc')->get() as $key => $recipe) {
            $recipe->categoria = CategoriaReceta::where('id',$recipe->id_categoria)->first()->nombre;
            $ingredient = $recipe->ingredientes;
            $result['receta '.$key] = [
                'receta' => $recipe, 
                'NumRecetas' => Receta::where('user_nick', $user->nick)->count(),
                'Num_comentarios' => Comentario::where('id_receta',$recipe->id)->count(),
                'Num_sugerencias' => Sugerencia::where('id_receta',$recipe->id)->count(),
            ];
        }
        $result = new Collection($result);
        return $this->showAll($result);
    }

    /**
     * recetas de usuarios que sigo
     * 
     * @param  App\IngredientesReceta  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function followingUsersRecepies(Usuario $user) {
        $result = [];
        
        $key = 1;
        foreach ($user->recetas as $recipe) {

            $recipe->categoria = CategoriaReceta::where('id',$recipe->id_categoria)->first()->nombre;
            $recipe->ingredientes;
            $result['receta '.$key] = [
                'usuario' => $user->nick,
                'receta' => $recipe,
                'Num_comentarios' => Comentario::where('id_receta',$recipe->id)->count(),
                'Num_sugerencias' => Sugerencia::where('id_receta',$recipe->id)->count(),
            ];
            $key++;
        }

        foreach ($user->following as $key => $userFollow) {

            foreach ($userFollow->recetas as $key => $recipe) {

                $recipe->categoria = CategoriaReceta::where('id',$recipe->id_categoria)->first()->nombre;
                $recipe->ingredientes;
                $result['receta '.$key] = [
                    'usuario' => $userFollow->nick,
                    'receta' => $recipe,
                    'Num_comentarios' => Comentario::where('id_receta',$recipe->id)->count(),
                    'Num_sugerencias' => Sugerencia::where('id_receta',$recipe->id)->count(),
                ];
                
            }
            $key++;
        }
        
        $result = new Collection($result);
        $result->sortByDesc('created_at');
        return $this->showAll($result);
    }

    /**
     * recetas de una categoria
     * 
     * @param  App\IngredientesReceta  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function categoryRecepies(CategoriaReceta $categoria) {
        $result = [];
        foreach ($categoria->recetas()->orderBy('created_at')->get() as $key => $recipe) {
            $recipe->categoria = $categoria->nombre;
            $ingredient = $recipe->ingredientes;
            $result['receta '.$key] = [
                'receta' => $recipe,
                'Num_comentarios' => Comentario::where('id_receta',$recipe->id)->count(),
                'Num_sugerencias' => Sugerencia::where('id_receta',$recipe->id)->count(),
            ];
        }
        $result = new Collection($result);
        return $this->showAll($result);
    }
}

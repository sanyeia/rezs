<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriaReceta;

class CategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index','show']);
    }

    public function index() {
        $categorias = CategoriaReceta::all();
        return response($categorias);
    }

    public function store(Request $request) {
        if($request->has('categoria')){
            $categoria = new CategoriaReceta();
            $categoria->fill($request->categoria);
            $categoria->save();
            return response($categoria);
        } else{
            return response('Bad Request', 400);
        }
    }

    public function show(CategoriaReceta $categorium) {
        return response($categorium);
    }

    public function update(Request $request, CategoriaReceta $categorium) {
        if($request->has('categoria')){
            $categorium->fill($request->categoria);
            $categorium->save();
            return response($categorium);
        } else{
            return response('Bad Request', 400);
        }
    }

    public function destroy(CategoriaReceta $categorium) {
        // en teoria esto no deberia usarse nunca
        $categorium->delete();
        return response($categorium);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;
use App\Comentario;
use App\Usuario;

class ComentariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * crea un nuevo comentario de una receta.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Receta  $recipe
     * @param  App\Usuario  $user
     * @return \Illuminate\Http\Response
     */
    public function newComment(Request $request, Receta $recipe, Usuario $user)
    {
        if($request->has('comentario')){
            $comentario = new Comentario();
            $comentario->id_receta = $recipe->id;
            $comentario->user_nick = $user->nick;
            $comentario->fill($request->comentario);
            $comentario->save();

            actualizarCalificacion();

            return response($comentario);
        } else {
            return response('Bad Request gg', 400);
        }
    }

    /**
     * actualiza un comentario
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comentario $comentario)
    {
        if($request->has('comentario')){
            $comentario->fill($request->comentario);
            $comentario->save();
            actualizarCalificacion();
            return response($comentario);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * elimina un comentario de una receta.
     *
     * @param  App\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comentario $comentario)
    {
        $comentario->delete();
        return response($comentario);
    }

    private function actualizarCalificacion(Receta $recipe){
        $sum = 0;
        foreach ($recipe->comentarios as $comentario) {
            $sum += $comentario->calificacion;
        }
        $calificacion = $sum / $recipe->comentarios->count();
        $recipe->calificacion = $calificacion;
        return $calificacion;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Webpatser\Uuid\Uuid;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function imageStorage($imageid, $file){
        $path = public_path('img/' . $imageid . '.jpg');
        $url = asset('img/' . $imageid . '.jpg');
       \Image::make($file)
            ->encode('jpg', 75)
            ->save($path);
        return ['path'=>$path,'uuid'=>$imageid, 'url'=>$url];
    }
}

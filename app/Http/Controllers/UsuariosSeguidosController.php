<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuariosSeguidosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Seguir a un usuario 
     *
     * @return \Illuminate\Http\Response
     */
    public function followUser(Usuario $user, Usuario $followU)
    {
        if ($user->nick != $followU->nick) {
            $user->follow($followU);
            return response( (string)$user->nick . ' ahora sigue a '. (string)$followU->nick, 200);
        } else {
            return response('Operacion invalida', 400);
        }
    }

    /**
     * Dejar de seguir a un usuario 
     *
     * @return \Illuminate\Http\Response
     */
    public function unfollow(Usuario $user, Usuario $followU)
    {
        if ($user->nick != $followU->nick) {
            $user->unfollow($followU);
            return response($user->nick . ' dejo de seguir a '. $followU->nick, 200);
        } else {
            return response('Operacion invalida', 400);
        }  
    }
}

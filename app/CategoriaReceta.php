<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaReceta extends Model
{
    protected $table = 'categorias_recetas';
    public $timestamps = false;
    protected $fillable = ['nombre', 'descripcion'];

    public function recetas(){
        return $this->hasMany('App\Receta', 'id_categoria');
    }
    
}

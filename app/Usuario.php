<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Usuario extends Authenticatable
{
    use HasApiTokens;
    protected $table = 'usuarios';
    protected $primaryKey = 'nick';
    public $incrementing = false;
    protected $fillable = [
        'nick',
        'password',
        'nombre',
        'apellido',
        'fecha_nacimiento',
        'email'
    ];

    protected $hidden = ['password'];

    public function setPasswordAttribute($value){ 
      if (!empty($value)) $this->attributes['password'] = \Hash::make($value);
    }

    public function findForPassport($username) {
        return $this->where('nick', $username)->first();
    }
    
    public function getRouteKeyName(){
        return 'nick';
    }

    //relaciones
    public function recetas(){
        return $this->hasMany('App\Receta', 'user_nick', 'nick');
    }

    public function quejas(){
        return $this->hasMany('App\Queja', 'user_nick', 'nick');
    }

    public function sugerecias(){
        return $this->hasMany('App\Sugerencia', 'user_nick', 'nick');
    }

    public function comentarios(){
        return $this->hasMany('App\Comentario', 'user_nick', 'nick');
    }

    public function following(){
        return $this->belongsToMany('App\Usuario', 'usuarios_seguidos', 'user', 'follow')->select('nick');
    }

    public function followers(){
        return $this->belongsToMany('App\Usuario', 'usuarios_seguidos', 'follow', 'user')->select('nick');
    }

    public function follow(Usuario $user) {
        $this->following()->attach($user->nick);
    }

    public function unfollow(Usuario $user) {
        $this->following()->detach($user->nick);
    }

}

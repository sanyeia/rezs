<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientesReceta extends Model
{
    protected $table = 'ingredientes_recetas';
    protected $fillable = ['nombre','cantidad','tipo_cantidad'];
    
    public function recetas(){
        return $this->belongsTo('App\Receta', 'id_receta');
    }
}

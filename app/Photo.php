<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'element_id',
        'path',
        'url',
        'type_id',
        'status'
    ];


    

}


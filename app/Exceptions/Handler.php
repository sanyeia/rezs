<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Session\TokenMismatchException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof ValidationException) {
            $errors = $exception->validators->errors()->getMessages();
            return response($errors, 422);
        }
        if($exception instanceof ModelNotFoundException) {
            $model = class_basename($exception->getModel());
            $msg = 'Resource not found in '.$model;
            return response($msg, 404);
        }
        if($exception instanceof AuthenticationException) {
            return response('Unauthenticated', 401);
        }
        if($exception instanceof AuthorizationException) {
            return response('Unauthorized', 403);
        }
        if($exception instanceof NotFoundHttpException) {
            return response('exceptions.404.url', 404);
        }
        if($exception instanceof MethodNotAllowedHttpException) {
            return response('exceptions.405', 405);
        }
        if($exception instanceof HttpException) {
            $msg  = $exception->getMessage();
            $code = $exception->getStatuscode();
            return response($msg, $code);
        }
        if($exception instanceof QueryException) {
            $code = $exception->errorInfo[1];
            if($code == 1451)
                return response('exceptions.query', 409);
        }
            return parent::render($request, $exception);
        
        if(config('app.debug')){
                return response('exceptions.500', 500);
        } else {
            return parent::render($request, $exception);
        }

        if ($exception instanceof TokenMismatchException) {
            return response('exceptions.403', 403);
        }

    }
}

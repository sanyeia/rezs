<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//usuarios
Route::resource('user', 'UsuarioController', ['except'=>['create', 'edit']]);
Route::get('user/{user}/verify', 'UsuarioController@check');
Route::post('user/{user}/follow/{followU}', 'UsuariosSeguidosController@followUser');
Route::delete('user/{user}/unfollow/{followU}', 'UsuariosSeguidosController@unfollow');
Route::get('user/{user}/recepies', 'RecetaController@userRecepies');
Route::get('user/{user}/follow/recepies', 'RecetaController@followingUsersRecepies');

//categorias
Route::resource('categoria', 'CategoriaController', ['except'=>['create', 'edit']]);
Route::get('categorias/{categoria}/recetas', 'RecetaController@categoryRecepies');

//recetas
Route::resource('recipes', 'RecetaController', ['except'=>['create', 'edit', 'store']]);
Route::post('recipes/new/{user}', 'RecetaController@store');
Route::get('recipes/search/{recipe}', 'RecetaController@searchRecipe');
Route::get('ingredient/search/{ingredient}', 'RecetaController@searchIngredient');

//comentarios
Route::post('comentarios/{recipe}/{user}', 'ComentariosController@newComment');
Route::resource('comentarios', 'ComentariosController', ['only'=>['destroy','update']]);

//sugerencias
Route::post('recetas/{recipe}/user/{user}/sugerencias', 'SugerenciaController@store');
Route::get('recetas/{recipe}/sugerencias', 'SugerenciaController@show');

//imagen
Route::get('photos/{photo}', 'PhotoController@show'); // Obtener una imagen
